def calc(op, num1, num2)
	if op == "add"
		sum = num1.to_i + num2.to_i
	elsif op == "subs"
		sum = num1.to_i - num2.to_i
	elsif op == "mult"
		sum = num1.to_i * num2.to_i
	elsif op == "div"
		sum = num1.to_i / num2.to_i
	elsif op == "mod"
		sum = num1.to_i % num2.to_i
	else
		return puts "operation not supported"
	end
	
	return puts "calculation result: #{sum}"
end

print "first num: "
num1 = gets.chomp
print "second num: "
num2 = gets.chomp
print "operation: "
op = gets.chomp
calc(op, num1, num2)