-- Nomor 1

select * 
from pegawai 
where usia > 50;

-- Nomor 2

select cabang_id, total_pegawai 
from (select count(cabang_id) as total_pegawai, cabang_id 
	from pegawai 
	group by cabang_id 
	order by total_pegawai desc) as A;

-- Nomor 3

select divisi, jumlah_pegawai 
from (select count(divisi) as jumlah_pegawai, divisi 
	from pegawai 
	group by divisi) as A;

