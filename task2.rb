names = ['Ahmad', 'Tono', 'Tini', 'Bambang', 'Agus', 'Agung']

def chk_nm(arr)
	res = []
	arr.each do |item|
		if item.length % 2 == 1 && item[0] == 'A'
			res << item.upcase
		elsif item[0] == 'T'
			res << item.downcase
		else
			res << item.capitalize.reverse
		end
	end

	return puts res
end

chk_nm(names)