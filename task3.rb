students = {Alice: "75", Bob: "80", Candra: "100", Dede: "64", Eka: "90"}

def class_avg(hashh)

	avg = 0 
	flag = 0
	total_stdnts = hashh.length

	hashh.each_value do |val|
		avg += val.to_i
	end

	avg = avg / total_stdnts

	hashh.each_value do |val|
		if (val.to_i > avg)
			flag = flag + 1
		end
	end

	return puts "class average is #{avg}, with #{flag} student(s) with above average"
end

class_avg(students)

def chk_grade(val)

	grd = ""

	if val.to_i >= 80 && val.to_i <= 100
		grd = "A"
		return grd
	elsif val.to_i >= 70 && val.to_i < 80
		grd = "B"
		return grd 
	elsif val.to_i >= 60 && val.to_i < 70
		grd = "C"
		return grd
	elsif val.to_i >= 40 && val.to_i < 60
		grd = "D"
		return grd
	elsif val.to_i < 40
		grd = "E"
		return grd				
	end
			
end

def alpha_grade(hashh)

	res = []

	hashh.each do |key, val|
		grade = chk_grade(val)
		res << "#{key} grade is #{grade}"
	end

	return puts res
end

alpha_grade(students)