# SUPER CLASS =================
class Shape
	def area
	end

	def around
	end
end

# CHILD CLASS  ================
class Rectangle < Shape
	attr_accessor :height, :width

	def area
		return @height.to_i*@width.to_i
	end

	def around
		return 2*(@height.to_i+@width.to_i)
	end
end

class Circle < Shape
	attr_accessor :radius

	def area
		return (3.14*@radius.to_i*@radius.to_i).to_f.round(2)
	end

	def around
		return (2*3.14*@radius.to_i).to_f.round(2)
	end
end

# MEMILIH BENTUK ===============
# Pengecekan input
flag = false
while flag == false
	print "Choose shape to find area or around (C for Circle, R for Rectangle): "
	pilihan = gets.chomp
	if pilihan == 'C' || pilihan == 'R' || pilihan == 'c' || pilihan == 'r'
		flag = true
	else
		flag = false
	end
end

# MENGHITUNG BENTUK ============
# Jika Memilih Lingkaran
if pilihan == 'c' || pilihan == 'C'

	# Membuat Objek
	Lingkaran = Circle.new()
	print "Input radius: "
	Lingkaran.radius = gets.chomp

	# Pengecekan input
	flag = false
	while flag == false
		print "Choose to find area or circumference (A/C): "
		pilihHitung = gets.chomp
		if pilihHitung == 'A' || pilihHitung == 'C' || pilihHitung == 'a' || pilihHitung == 'c'
			flag = true
		else
			flag = false
		end
	end

	# Menghitung sesuai pilihan input
	if pilihHitung == 'A' || pilihHitung == 'a'
		luas = Lingkaran.area()
		puts "area =  #{luas}"
	elsif pilihHitung == 'C' || pilihHitung == 'c'
		kel = Lingkaran.around()
		puts "circumference =  #{kel}"
	end

# Jika Memilih Persegi
elsif pilihan == 'R' || pilihan == 'r'

	# Membuat Objek
	Persegi = Rectangle.new()
	print "Input height: "
	Persegi.height = gets.chomp
	print "Input width: "
	Persegi.width = gets.chomp

	# Pengecekan input
	flag = false
	while flag == false
		print "Choose to find area or circumference (A/C): "
		pilihHitung = gets.chomp
		if pilihHitung == 'A' || pilihHitung == 'C' || pilihHitung == 'a' || pilihHitung == 'c'
			flag = true
		else
			flag = false
		end
	end

	# Menghitung sesuai pilihan input
	if pilihHitung == 'A' || pilihHitung == 'a'
		luas = Persegi.area()
		puts "area =  #{luas}"
	elsif pilihHitung == 'C' || pilihHitung == 'c'
		kel = Persegi.around()
		puts "circumference = #{kel}"
	end
end