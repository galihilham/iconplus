# frozen_string_literal: true

require 'faker'

# Buat class Emoney
class Emoney
  attr_accessor :id, :saldo, :is_active

  def initialize
    @id = Faker::Number.number(digits: 6)
    @saldo = 0
    @aktivasi = false
  end

  def aktivasi
    @aktivasi ||= true
  end

  def cek_saldo
    puts "your balance is #{@saldo}"
  end

  def topup(nominal)
    if @aktivasi
      @saldo += nominal
    else
      puts 'E-money belum aktif'
    end
  end

  def transaction(nominal)
    if @aktivasi
      if @saldo >= nominal
        @saldo -= nominal
        puts "Saldo anda: #{@saldo}"
      else
        puts 'Saldo tidak mencukupi. Silakan top-up dahulu.'
      end
    else
      puts 'Silakan aktivasi terlebih dahulu.'
    end
  end
end

emon = Emoney.new

flag = false

puts "SELAMAT DATANG DI MENU E-MONEY. SILAHKAN PILIH MENU (ID: #{emon.id})"
while flag == false
  print '1: CEK SALDO | 2: STATUS AKTIVASI | '
  puts '3: TOP-UP | 4: TRANSAKSI | 5: KELUAR'
  print 'INPUT MENU: '
  input = gets.chomp
  if input == '1'
    emon.cek_saldo
  elsif input == '2'
    if emon.aktivasi
      puts 'STATUS E-MONEY TELAH AKTIF'
    else
      emon.aktivasi
      puts 'STATUS E-MONEY BELUM AKTIF. MENGAKTIVASIKAN E-MONEY'
    end
  elsif input == '3'
    print 'SILAHKAN ISI NOMINAL TOP-UP: '
    nominal_topup = gets.chomp.to_f
    emon.topup(nominal_topup)
    puts "SALDO TELAH DI TOP-UP. SALDO SEKARANG: #{emon.saldo}"
  elsif input == '4'
    print 'SILAHKAN ISI NOMINAL TRANSAKSI: '
    nominal_transaksi = gets.chomp.to_f
    emon.transaction(nominal_transaksi)
  elsif input == '5'
    puts 'TERIMA KASIH'
    flag = true
  else
    puts 'PILIHAN TIDAK ADA'
  end
end
